(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(auctex arduino-mode web-mode typescript-mode go-mode define-word pdf-tools git-link forge github-review magit htmlize org-present org-modern org-bullets org-roam pass centaur-tabs multiple-cursors undo-tree expand-region smartparens avy-zap smex dired-subtree all-the-icons-dired dired-narrow neotree perspective projectile ivy-posframe ivy-rich avy counsel yasnippet tree-sitter-langs tree-sitter orderless corfu all-the-icons centered-window hide-mode-line doom-modeline doom-themes general evil which-key use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
