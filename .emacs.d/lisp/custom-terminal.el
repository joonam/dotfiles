;;; custom-terminal.el --- terminal config
;;;
;;; Commentary:
;;;
;;; terminal
;;;
;;; Code:

(use-package vterm
  :defer t
  :bind*
  (("C-<return>" . vterm)))


;;; custom-terminal.el ends here
